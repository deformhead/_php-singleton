<?php

abstract class Singleton {

    protected static $instances = array( ) ;

    final public static function getInstance( ) {

        // get the called class name
        $class = get_called_class( ) ;

        // if there is no instance of the called class then create one
        if ( !array_key_exists( $class, self::$instances ) ) {

            // get the called class arguments
            $arguments = func_get_args( ) ;

            // prepare arguments
            foreach ( $arguments as &$argument ) {

                // get argument's representation
                $argument = var_export( $argument, true ) ;
            }

            // create an instance of the called class with its arguments
            $call = create_function(

                '$class',
                'return( new $class( ' . implode( ', ', $arguments ) . ' ) ) ;'
            ) ;

            // save the instance of the called class
            self::$instances[ $class ] = $call( $class ) ;
        }

        return ( self::$instances[ $class ] ) ;
    }

    final protected function __clone( ) { }
}
